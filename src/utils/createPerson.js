import { Education } from '../resources/Education';
import { Person } from '../resources/Person';

export function createPerson(response) {
  let educations = response.educations;
  const educationsArray = [];
  for (let i = 0; i < educations.length; i++) {
    educationsArray.push(
      new Education(
        educations[i].year,
        educations[i].title,
        educations[i].description
      )
    );
  }

  let person = new Person(
    response.name,
    response.age,
    response.description,
    educationsArray
  );
  return person;
}
