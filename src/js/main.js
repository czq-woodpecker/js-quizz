import { fetchData } from '../utils/fetchData';
import { createPerson } from '../utils/createPerson';
import { renderBasicInfo } from '../views/renderBasicInfo';
import { renderAboutMe } from '../views/renderAboutMe';
import { renderEducationInfo } from '../views/renderEducationInfo';

const URL = 'http://localhost:3000/person';

fetchData(URL)
  .then(response => {
    let person = createPerson(response);
    const { name, age, description, educations } = person;
    renderBasicInfo(name, age);
    renderAboutMe(description);
    renderEducationInfo(educations);
  })
  .catch(reject => {
    throw new Error(reject);
  });
