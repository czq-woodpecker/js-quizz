import $ from 'jquery';

export function renderEducationInfo(educations) {
  let educationUl = $('#education ul');
  educations.forEach(education => {
    educationUl.append(
      `
      <li>
        <h3 class="education_year">${education.year}</h3>
        <section class="education_item">
          <h3 class="education_title">${education.title}</h3>
          <p class="education_description">${education.description}</p>
        </section>
      </li>  
      `
    );
  });
}
