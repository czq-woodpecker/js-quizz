import $ from 'jquery';

export function renderAboutMe(description) {
  $('#person_description').html(description);
}
