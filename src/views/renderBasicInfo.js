import $ from 'jquery';

export function renderBasicInfo(name, age) {
  $('#person_name').html(name);
  $('#person_age').html(age);
}
